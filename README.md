# Brinell hardness test

Ontology for the Brinell hardness testing according to the DIN_EN_ISO_6506-1 standard.

Version 2.0.0, update based on the bfo+cco+mseo top-level ontologies.

- [Wiki page](https://gitlab.com/kupferdigital/wiki/-/wikis/Brinell-hardness-test)
